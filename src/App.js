import React, { Component } from "react";
import "./App.css";
import Header from "./components/header/Header";
import City from "./components/city/City";
import londonBg from "./images/london.png";
import berlinBg from "./images/berlin.png";
import amsterdamBg from "./images/amsterdam.png";

class App extends Component {
  state = {
    currentCity: "amsterdam",
    primaryColor: "#ff5476",
    weather: undefined
  };

  componentDidMount() {
    this.getWeatherData();
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevState.currentCity !== this.state.currentCity){
      this.getWeatherData()
    }
  }

  setCurrentCity = city => {
    this.setState({
      currentCity: city,
      primaryColor: this.setPrimaryColor(city)
    });
  };

  setPrimaryColor = city => {
    switch (city) {
      case "london":
      case "amsterdam":
        return "#ff5476";
      case "berlin":
        return "#00bbd3";
    }
  };

  getCityCode = city => {
    switch (city) {
      case "london":
        return 2643743;
      case "amsterdam":
        return 2759794;
      case "berlin":
        return 2950158;
    }
  };

  getCityBg = city => {
    switch (city) {
      case "london":
        return londonBg;
      case "berlin":
        return berlinBg;
      case "amsterdam":
        return amsterdamBg;
    }
  };

  toHebrew = name => {
    switch (name) {
      case "amsterdam":
        return "אמסטרדם";
      case "berlin":
        return "ברלין";
      case "london":
        return "לונדון";
    }
  };

  getWeatherData = () => {
    fetch(
      `http://localhost:8888?id=${this.getCityCode(this.state.currentCity)}`
    )
      .then(res => res.json())
      .then(weatherJson => {
        this.setState({ weather: weatherJson });
      });
  };

  render() {
    const { currentCity, primaryColor, weather } = this.state;
    return (
      <div className="App">
        <Header
          toHebrew={this.toHebrew}
          currentCity={currentCity}
          primaryColor={primaryColor}
          setCurrentCity={this.setCurrentCity}
        />
        <City
          toHebrew={this.toHebrew}
          currentCity={currentCity}
          primaryColor={primaryColor}
          weather={weather}
        />
      </div>
    );
  }
}

export default App;
