const express = require("express");
const port = 8888;
const hostname = "127.0.0.1";
var cors = require("cors");
const fetch = require("node-fetch");

const app = express();
app.use(cors());

const appid = "bce466bf57fde0a1209b7617563ff19a";

const requestHandler = (req, res) => {
  const weatherUrl = `https://api.openweathermap.org/data/2.5/weather?id=${
    req.query.id
  }&appid=${appid}&units=metric`;

    fetch(weatherUrl)
    .then(res => res.json())
    .then(json => res.send({
        humidity: json.main.humidity,
        temp: json.main.temp,
        precipitation: json.main.precipitation,
        icon: json.weather[0].icon,
        wind: json.wind.speed * 3.6 // from mps to kph
    }));
};

app.get("/", requestHandler);

app.listen(port, hostname, () =>
  console.log(`app listening on port ${port}!`)
);
