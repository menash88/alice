import React from "react";
import "./menu.css";

function Menu(props) {
  const cities = ["amsterdam", "berlin", "london"];

  const getUnderlinePosition = city =>{
    switch (city) {
      case "amsterdam":
        return "0";
      case "berlin":
        return "42%";
      case "london":
        return "80%"
    }
  }

  const underlineStyle = {
    borderColor: props.primaryColor,
    right: getUnderlinePosition(props.currentCity)
  };

  return (
    <div className="menu">
      {cities.map(city => (
        <div
          key={city}
          onClick={() => props.setCurrentCity(city)}
          style={{ textAlign: "center" }}
        >
          {props.toHebrew(city)}
        </div>
      ))}
      <div className="underline" style={underlineStyle} />
    </div>
  );
}

export default Menu;
