import React from "react";
import Menu from "../menu/Menu";
import logo from '../../images/logo.png'
import "./header.css"

function Header(props) {
  return (
    <header>
      <Menu {...props} />
      <div className="other">
          <span>כל הערים</span>
          <img src={logo}/>
      </div>
    </header>
  );
}

export default Header;
