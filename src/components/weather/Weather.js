import React from "react";
import "./weather.css";

function Weather(props) {
  const { temp, precipitation, humidity, wind, icon } = props.weather;
  const iconUrl = `http://openweathermap.org/img/w/${icon}.png`;

  return (
    <div className="weather">
      <h3 className="subtitle" style={{ color: props.primaryColor }}>
        מזג אוויר עכשיו
      </h3>
      <div className="details">
        <div className="icon-container">
          <img className="icon-image" src={iconUrl} />
        </div>
        <div className="weather-text">
          <div className="temp">
            <span>{Math.round(temp)}</span>
            <span>&deg;</span>
          </div>
          <div className="weather-info">
            {precipitation && (
              <p>
                <span>%</span>
                <span>Precipitaion: {precipitation}</span>
              </p>
            )}
            <p>
              <span>%</span>
              <span>Humidity: {humidity}</span>
            </p>
            <p>
              <span>Wind: {Math.round(wind)}</span> <span>km</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Weather;
