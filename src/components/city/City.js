import React from "react";
import londonBg from "../../images/london.png";
import berlinBg from "../../images/berlin.png";
import amsterdamBg from "../../images/amsterdam.png";
import "./city.css";
import Weather from "../weather/Weather";
import posed, { PoseGroup } from "react-pose";

function City(props) {
  const Img = posed.img({
    enter: { opacity: 1, y: 0 },
    exit: { opacity: 0, y: 100 }
  });

  const getCityBg = city => {
    switch (city) {
      case "london":
        return londonBg;
      case "berlin":
        return berlinBg;
      case "amsterdam":
        return amsterdamBg;
    }
  };

  const text = `לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית מוסן מנת.
     להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך.
      גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, קולהע צופעט למרקוח איבן איף,
      ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו,
      דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה.
      לפמעט מוסן מנת. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף.
      זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.
    קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר,
    בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח `;

  return <div className="city">
      <div className="info">
        <div className="about">
          <h3 className="subtitle" style={{ color: props.primaryColor }}>
            קצת על העיר
          </h3>
          <p className="text">{text}</p>
        </div>
        {props.weather && <Weather primaryColor={props.primaryColor} weather={props.weather} />}
      </div>
      <div className="visual">
        <h1 className="name" style={{ color: props.primaryColor }}>
          {props.toHebrew(props.currentCity)}
        </h1>
        <PoseGroup>
          <Img key={props.currentCity} className="city-bg" src={getCityBg(props.currentCity)} />
        </PoseGroup>
      </div>
    </div>;
}

export default City;
